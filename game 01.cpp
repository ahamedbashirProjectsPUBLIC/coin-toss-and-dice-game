/*Gaming project of Coin toss(Heads and Tails) and Dice(21 points).
The program never ends, will not close even if the player decide to quit or stop.*/
// Author: Bashir Ahamed
// Spring 2015, CSC 172


#include<iostream>
#include<fstream>
#include<string> 
#include<cstdlib> 
#include<ctime>

using namespace std;

void game1(); //Function for Coin game(Heads and Tails).
char coin();
void game2();  //Function for Dice game(21)
int diceRoll(),total=0;
int select;
char play;
char key;
string name;
ofstream fout;

int main()
{
	cout<<"Welcome! You are here to play a game.\nThe game will never exit. If you want to exit the game, press 'control+C'"<<endl;
	
	for(int i=1;i>0;i++)
	{

	cout<<"Do you want to play,(enter Y to play): ";
	cin>>play;
	if(play=='Y'||play=='y')
	{
	
	cout<<"Enter your name: ";
	cin>>name;
//	fout.open("gamers.txt",ios::app); //to print the name of the player on file, appended.
	fout.open("gamers.txt");
	if(fout.fail())
	{
		cout<<"Output file opening failed."<<endl;
		break;
	}
	
	fout<<name<<" is playing."<<endl;
	cout<<name<<" "<<"is playing the Game."<<endl;
	
	cout<<"Choose Which game you want to play (press 1,2 or 3)\n1) Heads and Tails\n2) Game of 21\n3) Quit"<<endl;
	cout<<"Select your game by entering 1,2 or 3: ";
	cin>>select;
	cout<<"You selected game "<<select<<" to play."<<endl;
	cout<<"Do you want to continue your game?\nPress 'C' to continue\nPress 'P' to go back to the previous menu\nPress 'R' to the main menu"<<endl;
	cin>>key;
	do
	{
	
	if(key=='c'||key=='C')
	{
		cout<<"You pressed 'C' to continue to your previous selection.\n"<<endl;
		if(select==1)
		{
			cout<<"This is a game of Heads and Tails.\nIf you get two Heads in a row, you get 2 points.\nIf you get two Tails in a row, you lose 1 points.\nOtherwise there is no point won or lost. "<<endl;
			game1();
			
		}
		else if(select==2)
		{
			cout<<"This a game of dice 21 . You win if your total score is 21 (2 points will be added to your final score)\nlose if your total score is more than 21 (2 points will be deducted from your final score)\nThe game is draw if you quit before reaching 21."<<endl;
			game2();
			
		}
		else if(select==3)
		{
			fout<<"You quit playing any game."<<endl;
			cout<<"You selected to quit.\nReturning to main menu.\n"<<endl;
			break;
		}		
	}
	
	else if(key=='P'||key=='p')
	{
		cout<<"You pressed 'P' to return to the previous manu of game selection. Returning...\n"<<endl;
		cout<<"Choose Which game you want to play (press 1,2 or 3)\n1) Heads and Tails\n2) Game of 21\n3) Quit"<<endl;
		cout<<"Select your game by entering 1,2 or 3: "<<endl;
		cin>>select;
		cout<<"You selected game "<<select<<" to play."<<endl;
		cout<<"Do you want to continue your game?\nPress 'C' to continue\nPress 'P' to go back to the previous menu\nPress 'R' to the main menu"<<endl;
		cin>>key;
		
	}
	
	else if(key=='R'||key=='r')
	{
		cout<<"You pressed 'R' to return to the main screen. Returning...\n"<<endl;
		break;
	}
    }while(play=='Y'||play=='y');
    fout.close();
   }
   }
   
   return 1;
}

void game1() //function for heads and tails game.
{
	int score=0;
	char prev=' ',curr;
	
	do
	{
		curr=coin();
		if(curr=='H'&&prev=='H')
		{
			cout<<"You got two heads in a row, and won 2 points."<<endl;
			score=score+2;
			cout<<"Your score is: "<<score<<endl;
		}
		
		else if(curr=='T'&&prev=='T')
		{
			cout<<"You got two tails in a row, and lost 1 points."<<endl;
			score=score-1;
			cout<<"Your score is: "<<score<<endl;
		}
		else
		{
			cout<<"You didn't win or lose any point."<<endl;
			score=score+0;
			cout<<"Your score is: "<<score<<endl;
		}
		
		prev=curr;
		cout<<"Do you want to play?: ";
		cin>>play;
		if(play=='N'||play=='n')
		{
			cout<<"You pressed not to play. Returning to main menu.\n"<<endl;
			break;
		}
		
	}while(play!='N'||play!='n');
	fout<<"Game played Heads and Tails."<<endl<<"score: "<<score<<endl;	//prints final score on the file of the gamer.
}

char coin()   //get heads or tails. all the even numbers are Heads
{
	char heads='H',tails='T';
	srand(time(NULL));
	int i=rand()%10+1;
	
	if(i%2==1)
	{
		cout<<"You got Heads: "<<heads<<endl;
		return heads;
	}
	
	else
	{
		cout<<"You got Tails: "<<tails<<endl;
		return tails;
	}
}

void game2() //function for 21 dice game
{
	int score;
		
	do
	{
		diceRoll();
		
		if(total<21)
		{
			cout<<"Your current total is: ";
			cout<<total<<endl;
			cout<<"Do you want to play?: ";
			cin>>play;
			if(play=='N'||play=='n')
			{
				cout<<"Game is drawn"<<endl;
				score=total;
				cout<<"Your total score is: "<<total<<"\n"<<endl;
				break;
			}
		
		}
		else if(total==21)
		{
			cout<<"Your current total is: ";
			cout<<total<<endl;
			cout<<"You won the game by scoring exactly 21 points and 2 points will be added to your score."<<endl;
			score=total+2;
			cout<<"Your final total score is: "<<score<<endl;
			cout<<"Do you want to play again? (if yes, your total score will be counted from Zero): ";
			cin>>play;
			if(play=='N'||play=='n')
			{
				cout<<"\n"<<endl;
				break;
			}
			else
			{
				cout<<"\n"<<endl;
				total=0;
			}
			
		}
		else if(total>21)
		{
			cout<<"Your current total is: ";
			cout<<total<<endl;
			cout<<"You lost by exceeding 21 points and 2 points will be deducted from your score."<<endl;
			score=total-2;
			cout<<"Your final total score is: "<<score<<endl;
			cout<<"Do you want to play again?(if yes, your total score will be counted from zero): ";
			cin>>play;
			if(play=='N'||play=='n')
			{
				cout<<"\n"<<endl;
				break;
			}
			else
			{
				cout<<"\n"<<endl;
				total=0;
			}
			
		
		}
	    
	
	}while(play!='N'||play!='n');
	fout<<"Game played Dice 21."<<endl<<"score: "<<score<<endl;  //prints final score on the file of the gamer.
	
}
int diceRoll() //to get random output as a dice ranging from 1 to 6.
{
	int dice;
	srand(time(NULL));
	dice=(rand()%6)+1;
	total= total+dice;
	return total;
}
